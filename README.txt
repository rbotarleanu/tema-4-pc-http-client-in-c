Botarleanu Robert-Mihai
321CB
Tema 4 PC

	Pentru a rezolva tema am ales sa modularizez initializarea conexiunii cu serverul si 
a cererii de GET, facilitand astfel o recursiune clara. Adancimea maxima de cautare
se poate modifica usor schimband valoare directivei de precompilare #define MAXDEPTH.
	Astfel, dupa parsarea argumentelor primite de la linia de comanda se va crea un director
cu numele site-ului si se va schimba directorul curent de lucru in acesta.
	Se va pregati un fileDescriptor cu o noua conexiune cu serverul, se va parsa resursa
ceruta in cale de directoare si nume al resursei, se va pregati directoarele aferente 
prin apelul functiei mkdir -p si se va schimba directorul curent de lucru in acesta.
	Operatiile de parsare a unei cai o face functia "fname", iar crearea directoarelor
necesare si schimbarea directorului curent de lucru o face functia "prepare_client", in 
timp ce realizarea unei conexiuni noi cu serverul si alocarea unui file descriptor nou o va face functia "init()".
	In cadrul functiei send_get_request() se va trimite secventa de GET pregatita de procedura
"loadRequest". Se va folosi functia ReadLine[1] pentru a citi prima parte a raspunsului serverului,
care include mesajul de confirmare OK sau altfel un mesaj de eroare 40x. 
	In cazul unui mesaj de eroare, daca a fost selectata optiunea -o atunci se va scrie in
fisierul de log, altfel se vor ignora restul datelor trimise de server si se va incheia activitatea
pe conexiunea respectiva.
	Daca clientul primeste OK, atunci va pregati fisierul corespunzator paginii cerute si, daca
a fost selectata optiunea -r, se vor cauta toate link-urile de tip <a ... href="relative/path/file">
...</a>. Daca nu a fost selectata optiunea -e, atunci vort fi luate in considerare doar link-urile
care se termina cu html/htm, altfel se va adauga orice link care include o secventa ".extensie".
In ambele cazuri, se iau in considerare doar link-uri relative la server-ul cu care ne-am conectat,
orice referinta in afara "OUTSOURCELINK" care incepe cu "http://" va fi ignorata.
	O alta functie utila este "getType(char *name)" care va determina tipul resursei: pagina html
sau altceva. Daca name se termina cu html/htm atunci valoarea de return va fi "page"(0), altfel
"resource"(1).
	Functia send_get_request mai are pe langa socket-ul folosit, buffer-ul cu mesajul de GET si
numele fisierului/directorului si tipul resursei pe care o va cere si adancimea la care se afla
in interiorul cautarii recursive.


Referinte:
[1]	Functia ReadLine care citeste pana la \n din fileDescriptor sau pana se atinge limita
de octeti data ca argument a fost inclusa in rezolvarea Dl-ului Prof. Godza pentru laboratorul 
de HTTP, ea se poate gasi si in:
	(Listing 59-1, page 1201), an example program file from the book, The Linux Programming Interface. 
	(http://man7.org/tlpi/code/online/dist/sockets/read_line.c.html)

