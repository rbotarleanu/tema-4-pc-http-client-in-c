/**
    Nume: Botarleanu Robert-Mihai
    Grupa: 321CB
    Tema 4 PC
*/

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>

#include <netdb.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#define HTTP_PORT 80
#define MAXLEN 9000
#define MAXFPATH 256
#define MAXDEPTH 5
#define MAXBRANCHFACTOR 30

#define OUTSOURCELINK "http://"
#define JAVASCRIPTCALL "javascript:"
#define LINK "href=\""

#define CONFIRMATION "HTTP/1.1 200"

enum {false, true};
enum {page, resource};
typedef char bool;

//numele serverului
char host[MAXFPATH];
//directorul root din care se lucreaza
char wd[MAXFPATH];

//fisierul de log
FILE *logout;

//flaguri pentru optiunile de rulare
bool LOG = false;
bool EVERYTHING = false;
bool RECURSION = false;

//Directorul de root(directorul pe care se afla prima pagina din apelul programului).
char rootDir[MAXLEN] = "";

/**
    Incarca o comanda mkdir recursiva si o intoarce prin efect lateral.
    Comanda va fi sub forma:  mkdir -p "<file_path>"
*/
void mkdir_load(char *dir, char *cmd) {
    sprintf(cmd, "mkdir -p \"%s\"", dir);
}

/**
    Incaca un GET request catre server. Primeste ca parametru resursa ceruta,
    si intoarce prin efect lateral mesajul.
*/
void loadRequest(char *buffer, char* resource) {
    sprintf(buffer, "GET %s HTTP/1.1\nHost:%s\n\n", resource, host);
}

/**
    Citeste pana la '\n' sau pana la maxlen.
    Functie preluata de pe: http://man7.org/tlpi/code/online/dist/sockets/read_line.c.html
    This is sockets/read_line.c (Listing 59-1, page 1201), an example program file from the book,
    The Linux Programming Interface.
 */
ssize_t Readline(int sockd, void *vptr, size_t maxlen) {
    ssize_t n, rc;
    char    c, *buffer;

    buffer = vptr;

    for ( n = 1; n < maxlen; n++ ) {
        if ( (rc = read(sockd, &c, 1)) == 1 ) {
            *buffer++ = c;
            if ( c == '\n' )
            break;
        }
        else if ( rc == 0 ) {
            if ( n == 1 )
                return 0;
            else
            break;
        }
        else {
            if ( errno == EINTR )
            continue;
            return -1;
        }
    }
    *buffer = 0;
    return n;
}

/**
    Preia o cale si intoarce numele fisierului, modificand prin efect lateral
    numele directorului.
*/
char *fname(char *full_path, char *dir_path) {
    char *dir_ptr;
    char aux_dir[MAXFPATH], *aux_ptr = aux_dir;
    char *name_ptr = strrchr(full_path, '/');
    ++name_ptr;
    //name_ptr va fi pozitionat acum pe secventa de caractere aflata dupa ultimul
    //separator; calea de directoare va fi toata secventa de caractere pana la
    //name_ptr
    for(dir_ptr = full_path; dir_ptr != name_ptr-1; ++dir_ptr, ++aux_ptr) {
        *aux_ptr = *dir_ptr;
    }
    *aux_ptr = '\0';
    memset(dir_path, 0, sizeof(dir_path));
    //pentru a nu face referire la root, se adauga indicatorul de director curent
    sprintf(dir_path, ".");
    strcat(dir_path, aux_dir);
    return name_ptr;
}

/**
    Pregateste clientul pentru preluarea unui fisier de la server.
    Se determina directorul si numele fisierului. Se creaza directorul
    si se schimba directorul curent de lucru.
*/
char *prepare_client(char *URI, char* dir) {
    char cmd[MAXFPATH];
    char *path = strdup(URI);
    memset(dir, 0, sizeof(dir));
    char dirPath[MAXFPATH];
    char *fileName = fname(path, dirPath); //se determina numele fisierului
    //si directorul
    printf("Directory is:%s\n", dirPath);
    printf("opening file:%s\n", fileName);
    if(strlen(dirPath) > 2) {
        //se pregateste calea absoluta pentru director
        char fullPath[MAXFPATH];
        memset(fullPath, 0, sizeof(fullPath));
        sprintf(fullPath, "%s/%s", wd, dirPath+2);
        printf("Full path is:%s\n", fullPath);
        strcpy(dir, dirPath+1);
        //se pregateste apelul de mkdir -p
        mkdir_load(fullPath,cmd);
        printf("Cmd is :%s\n", cmd);
        system(cmd);
        //se schimba directorul curent
        char path_change[MAXFPATH];
        sprintf(path_change, "\"%s\"", fullPath);
        chdir(fullPath);
        printf("Now:%s\n", getcwd(fullPath, sizeof(fullPath)));
    }
    return fileName;
}

/**
    Va determina si intoarce numarul de referiri la alte pagini .html/.htm
    sau la resurse(daca optiunea -e a fost selectata).
    Va modifica prin efect lateral numarul de astfel de link-uri gasite.
*/
char **getSubResources(char *buffer, int *no_links)
{
      char **subPages = (char**)malloc(MAXFPATH * sizeof(char*));
      int linkNo = 0;
      //se cauta tag-ul href=
      char *ptr = strstr(buffer, LINK);
      char *aux_ptr = ptr;
      //daca nu s-a gasit, inseamna ca nu exista astfel de link-uri in buffer
      if(!ptr) return NULL;
      while(aux_ptr != NULL) {
        //se sare peste secventa href="
        aux_ptr = aux_ptr + strlen(LINK);
        char fileName[MAXFPATH], *aux_f = fileName;
        //se calea efectiva a link-ului
        for(; *aux_ptr != '\"' && *aux_ptr != ';' && *aux_ptr != '#' &&
         *aux_ptr != '\0'; ++aux_ptr, ++aux_f) {
            *aux_f = *aux_ptr;
        }
        *aux_f = '\0';
        //se adauga / daca nu exista deja
        if(fileName[0] != '/') {
            char *aux = strdup(fileName);
            sprintf(fileName, "/%s", aux);
        }
        //daca link-ul gasit nu este un apel la javscript sau un link
        //care incepe cu http:// si daca el are o extensie
        if(!strstr(fileName+1, JAVASCRIPTCALL)
          && !strstr(fileName+1, OUTSOURCELINK)) {
            char *file_type = strrchr(fileName, '.');
            if(file_type && (*(file_type-1) != '.')) {
                //se afla tipul daca nu a fost selectata optiunea -e
                //altfel se adauga in lista
                ++file_type;
                if(!EVERYTHING) {
                    if(!strncmp(file_type, "htm", 3) || !strncmp(file_type,"html",4)) {
                        printf("Got file:%s\n", fileName);
                        subPages[linkNo++] = strdup(fileName);
                    }
                } else {
                    printf("Got file:%s\n", fileName);
                    subPages[linkNo++] = strdup(fileName);
                }
            }
        }
        //se cauta urmatorul tag href
        aux_ptr = strstr(aux_ptr, LINK);
      }
      //se actualizeaza numarul de link-uri gasite si se intorc
      *no_links = linkNo;
      return subPages;
}

/**
    Determina tipul link-ului.
    Daca se termina in html/htm va intoarce valoarea page; altfel resource
*/
int getType(char *name) {
    char *tip = strrchr(name, '.');
    ++tip;
    int ftype = resource;
    if(!strncmp(tip, "html", 4) || !strncmp(tip, "htm", 3)) ftype = page;
    return ftype;
}

/**
    Trimite un request la server si apeleaza recursiv in continuare daca
    este cazul.
*/
int send_get_request(
    int sockfd, char sendbuf[], char* fileName, char* crtdir,
    int type, int depth)
{
    if(depth > MAXDEPTH) {return depth-1; }
    char subPages[MAXBRANCHFACTOR][MAXFPATH];
    int no_subPages = 0;
    printf("Getting resource:%s   crt_dir:%s\n", fileName, crtdir);

    char recvbuf[MAXLEN];
    int nbytes;
    char CRLF[3];
    bool err = false;

    memset(recvbuf, 0, sizeof(recvbuf));
    CRLF[0] = 13; CRLF[1] = 10; CRLF[2] = 0;
    //se adauga \r\t\0
    strcat(sendbuf, CRLF);
    //se trimite request-ul de GET
    printf("Trimit: %s", sendbuf);
    int newsockfd;
    //daca suntem in recursiune, pregatim o conexiune noua cu serverul
    if(depth > 1) { sockfd = init(); }
    write(sockfd, sendbuf, strlen(sendbuf));
    FILE *fout = NULL;
    //citim raspunsul initial si verificam daca am primit OK.
    nbytes=Readline(sockfd, recvbuf, sizeof(recvbuf));
    printf("Got:%s\n", recvbuf);
    if(strncmp(recvbuf, CONFIRMATION, strlen(CONFIRMATION))) {
        printf("Eroare la cerere:\n%s\n", recvbuf);
        if(LOG) {
            fprintf(logout, "%s\n%s\n", sendbuf, recvbuf);
        }
        fflush(logout);
        err = true;
    } else {
        //daca totul este in regula, pregatim fisier de scriere.
        fout = fopen(fileName, "wb");
        if(!fout) {
            printf("Cannot open file to resource.");
            if(LOG) fprintf(logout, "Cannot open file to resource.");
            fclose(fout);
            return depth;
        }
    }
    //citim din socket raspunsul serverului
    while ((nbytes=read(sockfd, recvbuf, sizeof(recvbuf)))>0){
        recvbuf[nbytes] = 0;
        //daca am primit eroare mesajele ce urmeaza vor fi ignorate
        if(err == true) {
            if(LOG) fwrite(recvbuf, sizeof(char), nbytes, logout);
            continue;
        }
        //altfel  actualizez numarul de link-uri gasite in buffer
        fwrite(recvbuf, sizeof(char), nbytes, fout);
        if(RECURSION && type == page) {
            int aux_dim = 0;
            char **newSubPages = getSubResources(recvbuf, &aux_dim);
            int j;
            for(j = 0; j < aux_dim; ++j, ++no_subPages) {
                strcpy(subPages[no_subPages], newSubPages[j]);
            }
        }
    }
    //daca am primit mesaj de eroare nu a fost deschis fisierul de scriere
    if(err == true) return;
    fclose(fout);
    //daca tipul paginii nu este html nu se mai cauta recursiv
    if(type == resource) return;
    if(type == page) {
        //daca a fost selectata optiunea -r
        if(RECURSION) {
            int i = 0;
            for(; i < no_subPages; ++i) {
                //se determina noul director si se apeleaza recursiv
                char req[MAXLEN];
                char sub_dir[MAXFPATH];
                char parentDir[MAXLEN], parentF[MAXLEN];
                char crtDir[MAXFPATH];
                strcpy(crtDir, "/");
                strcat(crtDir, crtdir+1);
                strcat(crtDir, subPages[i]);
                //se incarca GET request
                loadRequest(req, crtDir);
                //se creeaza noul director
                char *fname = prepare_client(crtDir, sub_dir);
                //se determina tipul(pentru o eventuala continuare a recursiunii)
                int ftype = getType(subPages[i]);
                //se apeleaza recursiv
                int rec = send_get_request(sockfd, req, fname,
                                           sub_dir, ftype, depth+1);
             }
        }
    }
    //intoarce adancimea
    return depth;
}

//initializeaza o conexiune cu serverul
int init() {
    int sockfd;
    int port = HTTP_PORT;
    struct sockaddr_in servaddr;
    char server_ip[15];

    strcpy(server_ip, host);
    if ( (sockfd = socket(PF_INET, SOCK_STREAM, 0)) < 0 ) {
        if(LOG) fprintf(logout, "Eroare la creare socket\n");
        printf("Eroare la  creare socket.\n");
        exit(-1);
    }

    //se construieste adresa serverului.
    struct hostent* hoststr = gethostbyname(host);
    if(hoststr == NULL){
            if(LOG) fprintf(logout, "Host unknown\n");
            herror("Host unknown");
            exit(-2);
    }

    struct in_addr ** list = (struct in_addr **)hoststr->h_addr_list;
    memset( (char *)&servaddr,0,sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(port);
    memcpy( (char *) &servaddr.sin_addr,list[0],hoststr->h_length);

    //se conecteaza la server
    if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0 ) {
        if(LOG) fprintf(logout, "Eroare la conectare\n");
        printf("Eroare la conectare\n");
        exit(-1);
    }
    //se intoarce noul file descriptor
    return sockfd;
}

int main(int argc, char **argv) {
    char sendbuf[MAXLEN];
    char rootDir1[MAXLEN];

    char serverName[MAXLEN], resource[MAXLEN];
    //se parseaza comanda utilizatorului
    int i;
    for(i = 1; i < argc; ++i) {
        //ultimul argument: numele serverului si pagina ceruta
        if(!strncmp(argv[i], "http://", 5)) {
            argv[i] = argv[i] + strlen("http://");
            char *position_ptr = strchr(argv[i], '/');
            strncpy(serverName, argv[i], position_ptr - argv[i]);
            strcpy(resource, position_ptr);
            printf("Get file:%s from: %s\n", resource, serverName);
            char makeDir[MAXLEN];
            sprintf(makeDir, "mkdir %s", serverName);
            system(makeDir);
            chdir(serverName);
            break;
        }
        //altfel se verifica care optiune a fost data ca argument
        if(!strcmp(argv[i], "-r")) RECURSION = true;
        if(!strcmp(argv[i], "-e")) EVERYTHING = true;
        if(!strcmp(argv[i], "-o"))
            {LOG = true; logout = fopen(argv[i+1], "w"); ++i;}
    }
    //se pastreaza directorul de lucru
    char aux[MAXFPATH];
    strcpy(wd, getcwd(aux, sizeof(aux)));
    strcpy(host, serverName);
    //se pregateste o conexiune cu serverul
    int sockfd = init();
    loadRequest(sendbuf, resource);
    //se construieste directorul si se parseaza numele paginii
    char *fileName = prepare_client(resource, rootDir1);
    strcpy(rootDir, rootDir1 + 1);
    printf("Rootdir: %s\n", rootDir);
    char initPath[MAXFPATH];
    sprintf(initPath, ".%s", rootDir);
    printf("Working Directory: %s\n", wd);
    //se determina tipul si se face cererea la server
    int type = getType(fileName);
    send_get_request(sockfd, sendbuf, fileName, initPath, type, 1);
    //se inchide conexiunea initiala si fisierul de log.
    close(sockfd);
    if(LOG) fclose(logout);
    return 0;
}

